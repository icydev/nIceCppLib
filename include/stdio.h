#pragma once
#include "stdarg.h"

typedef void FILE;

FILE* __nrt_get_stream(unsigned int id);

#define stdout (__nrt_get_stream(1))

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus
int printf(const char * __restrict format, ...);
int vprintf(const char * __restrict format, va_list args);

int fprintf(FILE * __restrict stream, const char * __restrict format, ...);
int vfprintf(FILE * __restrict stream, const char * __restrict format, va_list args);

int sprintf(char * __restrict buffer, const char * __restrict format, ...);
int vsprintf(char * __restrict buffer, const char * __restrict format, va_list args);
#ifdef __cplusplus
}
#endif // __cplusplus

#pragma once

typedef char* va_list;

#define _INTSIZEOF(n) ((sizeof(n) + sizeof(int) - 1) & ~(sizeof(int) - 1))

#define va_start(ap, parm_n) ((void)(ap = (va_list)&parm_n  + _INTSIZEOF(parm_n)))
#define va_arg(ap, T) (*(T*)((ap += _INTSIZEOF(T)) - _INTSIZEOF(T)))
#define va_end(ap) ((void)(ap = (va_list)0))

#undef _INTSIZEOF

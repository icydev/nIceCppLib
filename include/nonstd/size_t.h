#pragma once

#if defined(_M_X64) || defined(__x86_64__)
    typedef unsigned long long size_t;
#else
	typedef unsigned long size_t;
#endif

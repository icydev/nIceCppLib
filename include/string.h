#include "nonstd/size_t.h"
#include "nonstd/NULL.h"

char* strcpy(char* dest, const char* src);
char* strncpy(char* dest, const char* src, size_t count);
char* strcat(char* dest, const char* src);
char* strncat(char* dest, const char* src, size_t count);
size_t strxfrm(char* dest, const char* src, size_t count);

char* strcpy(char* dest, const char* src) {
	while (*src != '\0') *dest++ = *src++;
	*dest = *src;
	return dest;
}

char* strncpy(char* dest, const char* src, size_t count) {
	if (count == 0) return dest;
	while (*src != '\0' && count > 1) {
		*dest++ = *src++;
		--count;
	}
	*dest = *src;
	return dest;
}

char* strcat(char* dest, const char* src) {
	while (*dest != '\0') ++dest;
	while (*src != '\0') *dest++ = *src++;
	*dest = '\0';
	return dest;
}

char* strncat(char* dest, const char* src, size_t count) {
	if (count == 0) return dest;
	while (*dest != '\0') ++dest;
	while (*src != '\0' && count > 1) {
		*dest++ = *src++;
		--count;
	}
	*dest = '\0';
	return dest;
}

void* memcpy(void* dest, const void* src, size_t count) {
	char* cdest = reinterpret_cast<char*>(dest);
	const char* csrc = reinterpret_cast<const char*>(src);

	while (count > 0) {
		*cdest = *csrc;
		--count;
		++cdest;
		++csrc;
	}

	return dest;
}

void* memset(void* ptr, int value, size_t num) {
	char* cptr = reinterpret_cast<char*>(ptr);
	char cvalue = static_cast<char>(value);
	while (num > 0) {
		*cptr = cvalue;
		--num;
		++cptr;
	}

	return ptr;
}

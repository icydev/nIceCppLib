#include "../include/stdio.h"
#ifdef _WIN32
//undef things to avoid compiler warnings when Windows.h redefines them
#undef va_start
#undef va_arg
#undef va_end

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#endif

FILE* __nrt_get_stream(unsigned int id) {
	if (id == 1) {
		return GetStdHandle(STD_OUTPUT_HANDLE);
	}
	return NULL;
}

int __nrt_write_stream(FILE* stream, const char* str, int len, int* totalWritten) {
	DWORD written;
	WriteFile(stream, str, len, &written, NULL);
	if (!written) return -1;
	*totalWritten += written;
	return written;
}

int __nrt_write_stream_perc(FILE* stream, int* totalWritten) {
	return __nrt_write_stream(stream, "%", 1, totalWritten);
}

int printf(const char * __restrict format, ...) {
	va_list args;
	va_start(args, format);
	int ret = vfprintf(stdout, format, args);
	va_end(args);
	return ret;
}

int vprintf(const char * __restrict format, va_list args) {
	return vfprintf(stdout, format, args);
}

int fprintf(FILE * __restrict stream, const char * __restrict format, ...) {
	va_list args;
	va_start(args, format);
	int ret = vfprintf(stream, format, args);
	va_end(args);
	return ret;
}

int vfprintf(FILE * __restrict stream, const char * __restrict format, va_list args) {
	int ret = 0;

	const char *last = format;
	while (*format != '\0') {
		if (*format == '%') {
			if (format > last) {
				if (__nrt_write_stream(stream, last, format - last, &ret) < 0) {
					return -1;
				}
			}

			++format;
			if (*format == '%') {
				int written = __nrt_write_stream(stream, format, 1, &ret);
				if (written < 0) return written;
				format += written;
			} else {
				last = format;
				while (*format < 'A' || *format > 'z') ++format;
				if (*format == 'c') {
					//todo: add support for l argument
					__nrt_write_stream(stream, &va_arg(args, char), 1, &ret);
				}
			}

			last = format + 1;
		} else {
			++format;
		}
	}

	if (format > last) {
		DWORD written;
		WriteFile(stream, last, format - last, &written, NULL);
		if (!written) return -1;
		ret += written;
	}
	return ret;
}

int sprintf(char * __restrict buffer, const char * __restrict format, ...) {
	return 0;
}

int vsprintf(char * __restrict buffer, const char * __restrict format, va_list args) {
	return 0;
}
